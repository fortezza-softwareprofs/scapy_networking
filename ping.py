#!/usr/bin/env python3
from scapy.all import IP, ICMP, sr1, RandShort

def ping(dst, iface):
    ping_pkt = IP(dst=dst) / ICMP(id=RandShort())
    pong_pkt = sr1(ping_pkt, verbose=0, timeout=3, iface=iface)
    if not pong_pkt:
        print(f'{dst}: timeout')
    elif pong_pkt[ICMP].type == 0:
        print(f'{dst}: echo-reply received from {pong_pkt[IP].src}')
    else:
        print(f'{dst}: unexpected reply {pong_pkt[ICMP].type} received from {pong_pkt[IP].src}')

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('hostname')
    parser.add_argument('--iface', default=None)
    args = parser.parse_args(sys.argv[1:])
    ping(args.hostname, iface=args.iface)
