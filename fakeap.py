#!/usr/bin/env python3
from scapy.all import Dot11, Dot11Beacon, Dot11Elt, RadioTap, RandMAC, sendp

def fakeap(ssid, bssid, iface):
    print(f'[*] SSID: {ssid}')
    print(f'[*] BSSID: {bssid}')
    print(f'[*] Interface: {iface}')
    dot11 = Dot11(type=0, subtype=8, addr1='ff:ff:ff:ff:ff:ff', addr2=bssid, addr3=bssid)
    ess = Dot11Elt(ID='SSID', info=ssid, len=len(ssid))
    frame = RadioTap() / dot11 / Dot11Beacon(cap='ESS') / ess
    sendp(frame, iface=iface, inter=0.2, loop=1, monitor=True)

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('ssid', help='Fake SSID')
    parser.add_argument('--bssid', help='BSSID', default=str(RandMAC()))
    parser.add_argument('--iface', help='Network interface')
    args = parser.parse_args(sys.argv[1:])
    sys.exit(fakeap(args.ssid, args.bssid, args.iface))
